// https://on.cypress.io/api

describe("Register Form Test", () => {
  it("visits the register form url", () => {
    cy.visit("/register-form");
    cy.get("form");
  });

  it("fills the form properly", () => {
    cy.visit("/register-form");
    cy.get("input").eq(0).type("username");
    cy.get("input").eq(1).type("email@email.com");
    cy.get("input").eq(2).type("asdasdas1!");
    cy.get("input").eq(3).type("asdasdas1!");
    cy.get("button").click();
    cy.get(".app-toast").should("be.visible");
    cy.location().should((location) => {
      expect(location.pathname).to.eq("/user-details");
    });
  });
});
