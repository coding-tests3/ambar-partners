import type { AppToastProps } from "@/components/AppToast.vue";
import { defineStore } from "pinia";
import { ref } from "vue";

interface ToastProps {
  timeout: number;
}

type ToastConfig = AppToastProps & ToastProps;

interface Toast extends ToastConfig {
  id: number;
}

export const useToastStore = defineStore("toast", () => {
  const state = ref<Array<Toast>>([]);

  const createToast = (toast: ToastConfig) => {
    const id = state.value.length;
    state.value.push({
      id,
      ...toast,
    });

    setTimeout(() => {
      removeToast(id);
    }, toast.timeout);
  };

  const showSuccess = (
    message: string,
    { timeout }: ToastProps = { timeout: 2000 },
  ) => {
    createToast({
      message,
      type: "success",
      timeout,
    });
  };

  const removeToast = (id: number) => {
    state.value = state.value.filter((toast) => toast.id !== id);
  };

  return {
    state,
    showSuccess,
    removeToast,
  };
});
