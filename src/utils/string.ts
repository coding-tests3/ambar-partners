export const mask = (format: string) => (value?: string) => {
  if (!value) return "";

  let i = 0;
  const v = value.toString();
  return format.replace(/#/g, () => v[i++]);
};

export const maskCreditCard = mask("####-####-####-#####");
