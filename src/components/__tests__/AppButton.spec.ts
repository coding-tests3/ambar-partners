import { mount } from "@vue/test-utils";
import { describe, expect, it } from "vitest";
import AppButton from "../AppButton.vue";

describe("AppButton", () => {
  it("renders properly", () => {
    const wrapper = mount(AppButton, {
      props: { label: "Button example" },
    });
    expect(wrapper.text()).toContain("Button example");
  });

  it("should call click properly", async () => {
    const wrapper = mount(AppButton, {
      props: {
        label: "Button example",
      },
    });

    await wrapper.trigger("click");
    expect(wrapper.emitted()).toHaveProperty("click");
  });

  it("should not emit click event when button is disabled", async () => {
    const wrapper = mount(AppButton, {
      props: {
        label: "Button example",
        disabled: true,
      },
    });

    await wrapper.trigger("click");
    expect(wrapper.emitted()).not.toHaveProperty("click");
  });
});
