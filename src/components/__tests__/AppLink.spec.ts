import { RouteName } from "@/router";
import { shallowMount } from "@vue/test-utils";
import { describe, expect, it } from "vitest";
import AppLink from "../AppLink.vue";

describe("AppLink", () => {
  it("renders properly", () => {
    const wrapper = shallowMount(AppLink, {
      props: { to: RouteName.Intro },
      global: {
        mocks: {
          $t: () => "Link example",
        },
      },
    });
    expect(wrapper.text()).toContain("Link example");
  });
});
