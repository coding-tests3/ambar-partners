import { mount } from "@vue/test-utils";
import { describe, expect, it } from "vitest";
import AppToast from "../AppToast.vue";

describe("AppToast", () => {
  it("renders properly", () => {
    const wrapper = mount(AppToast, {
      props: { message: "Toast example", type: "error" },
    });
    expect(wrapper.text()).toContain("Toast example");
  });

  it("should show background-color properly", async () => {
    const wrapper = mount(AppToast, {
      props: { message: "Toast example", type: "error" },
    });
    expect(wrapper.text()).toContain("Toast example");
    expect(wrapper.classes()).toContain("bg-negative");

    await wrapper.setProps({ type: "success" });
    expect(wrapper.classes()).toContain("bg-positive");
  });
});
