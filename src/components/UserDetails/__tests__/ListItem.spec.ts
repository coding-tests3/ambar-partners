import { describe, expect, it } from "vitest";

import { mount } from "@vue/test-utils";
import AppSkeleton from "../../AppSkeleton.vue";
import ListItem from "../ListItem.vue";

describe("ListItem", () => {
  it("renders properly", () => {
    const wrapper = mount(ListItem, {
      props: { label: "Label example", value: "Value example" },
    });
    expect(wrapper.text()).toContain("Label example");
    expect(wrapper.text()).toContain("Value example");
  });

  it("show skeleton when loading", () => {
    const wrapper = mount(ListItem, {
      props: { label: "Label example", value: "Value example", loading: true },
    });
    expect(wrapper.text()).toContain("Label example");
    expect(wrapper.text()).not.toContain("Value example");
    expect(wrapper.findComponent(AppSkeleton)).toBeTruthy();
  });
});
