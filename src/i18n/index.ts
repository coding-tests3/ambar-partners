import { createI18n } from "vue-i18n";
import en from "./messages/en";
import es from "./messages/es";

const messages = {
  es,
  en,
};

export type MessageLanguages = keyof typeof messages;
export type MessageSchema = (typeof messages)["en"];

export default createI18n({
  legacy: false,
  locale: import.meta.env.VUE_APP_I18N_LOCALE || "en",
  fallbackLocale: import.meta.env.VUE_APP_I18N_FALLBACK_LOCALE || "en",
  messages,
  datetimeFormats: {
    en: {
      short: {
        day: "numeric",
        month: "2-digit",
        year: "numeric",
      },
    },
    es: {
      short: {
        day: "numeric",
        month: "2-digit",
        year: "numeric",
      },
    },
  },
});
