import { RouteName } from "@/router";

export default {
  router: {
    [RouteName.Intro]: "Introducción",
    [RouteName.RegisterForm]: "Reto 1: Formulario",
    [RouteName.UserDetails]: "Reto 2: Diseño",
    [RouteName.EventCalendar]: "Reto 3: Calendario",
  },

  validations: {
    email: "Debe ser una dirección de correo electrónico válida",
    equals: "Los valores no coinciden",
    format: "Formato no válido",
    required: "El campo es obligatorio",
  },

  calendar: {
    today: "Hoy",
    weekdays: ["D", "L", "M", "X", "J", "V", "S"],
  },

  label: {
    age: "Edad",
    previous: "Anterior",
    next: "Siguiente",
  },
};
