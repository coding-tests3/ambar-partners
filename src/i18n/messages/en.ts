import { RouteName } from "@/router";

export default {
  router: {
    [RouteName.Intro]: "Introduction",
    [RouteName.RegisterForm]: "Challenge 1: Form",
    [RouteName.UserDetails]: "Challenge 2: Design",
    [RouteName.EventCalendar]: "Challenge 3: Calendar",
  },

  validations: {
    email: "Should be a valid email",
    equals: "Values do not match",
    format: "Invalid format",
    required: "Field is mandatory",
  },

  calendar: {
    today: "Today",
    weekdays: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
  },

  label: {
    previous: "Previous",
    next: "Next",
    age: "1 year|{age} years",
  },
};
