import { axiosRequestAdapter } from "@alova/adapter-axios";
import { createAlova } from "alova";
import VueHook from "alova/vue";
import mockAdapter from "./mocks";

export default createAlova({
  baseURL: "https://api.ambarpartners.com",
  statesHook: VueHook,
  requestAdapter: import.meta.env.DEV ? mockAdapter : axiosRequestAdapter(),
  responded: (response) => response.data,
});
