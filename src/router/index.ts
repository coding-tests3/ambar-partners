import { createRouter, createWebHistory } from "vue-router";
export enum RouteName {
  Intro = "INTRO",
  RegisterForm = "REGISTER_FORM",
  UserDetails = "USER_DETAILS",
  EventCalendar = "EVENT_CALENDAR",
}

export const RoutePath: Record<RouteName, string> = {
  [RouteName.Intro]: "/",
  [RouteName.RegisterForm]: "/register-form",
  [RouteName.UserDetails]: "/user-details",
  [RouteName.EventCalendar]: "/event-calendar",
};

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      component: () => import("../layouts/MainLayout.vue"),
      children: [
        {
          path: RoutePath.INTRO,
          name: RouteName.Intro,
          component: () => import("../views/IntroView.vue"),
        },
        {
          path: RoutePath.REGISTER_FORM,
          name: RouteName.RegisterForm,
          component: () => import("../views/RegisterFormView.vue"),
        },
        {
          path: RoutePath.USER_DETAILS,
          name: RouteName.UserDetails,
          component: () => import("../views/UserDetailsView.vue"),
        },
        {
          path: RoutePath.EVENT_CALENDAR,
          name: RouteName.EventCalendar,
          component: () => import("../views/EventCalendarView.vue"),
        },
      ],
    },
  ],
});

export default router;
