import type { Config } from "tailwindcss";
import defaultColors from "tailwindcss/colors";

const colors = {
  ...defaultColors,
  primary: "#25272D",
  secondary: "#FFAE00",
  negative: defaultColors.red["600"],
  positive: defaultColors.green["600"],
};

const safelist = Object.keys(colors).flatMap((colorName) => {
  const color = colors[colorName];
  const mapColor = (name: string) => [`bg-${name}`, `text-${name}`];

  if (typeof color === "string") return mapColor(colorName);
  return Object.keys(color)
    .map((code) => `${colorName}-${code}`)
    .flatMap(mapColor);
});

export default {
  content: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  theme: {
    extend: {
      colors,
    },
  },
  safelist,
  plugins: [],
} satisfies Config;
